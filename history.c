#include "history.h"

enum __operation
{
        INS,
        DEL
};

struct __history_node
{
        struct __history_node *prev;
        enum __operation op;
        piece *pce;
};

struct __history
{
        struct __history_node *head;
        struct __history_node *curr;
};

struct __history
*history_init(void)
{
        struct __history *new_hist = malloc(sizeof(struct __history));
        if (new_hist == NULL)
        {
                new_hist->head = NULL;
                new_hist->curr = NULL;
        }
        return new_hist;
}

struct __history_node
*history_push(hist, op, pce)
struct __history *hist;
enum __operation op;
piece *pce;
{
        history_cancel_redos(hist);
        /* TODO actually add the thing */
        /* TODO fix the free/insertion history/piecetab dilemma */
}

/* TODO history_revise */
