/** @file piecetab.c
 *  This source file is responsible for the piecetab data structure.
 *  @copyright Copyright Rasmus Jepsen, George White (c) 2018.
 *             All rights reserved.
 *             Distributed under the MIT License.
 *             See file LICENSE in the root of this source code.
 */

#include "piecetab.h"
#include <stdlib.h>
#include <stdio.h>

/** Enumerates the type of a struct __piece */
enum __buf_type
{
        MEM, /**< The type for a struct __piece that has buf in memory */
        FIL  /**< Indicates that a struct __piece is part of the file buffer */
};

/** Stores a single piece for a struct __piecetab
 *  @note These pieces make a doubly-linked circular linked list.
 *  @see piece in piecetab.h
 */
struct __piece
{
        /** The offset into the file if type == FIL,
         *  offset is 0 otherwise
         */
        size_t offset;
        size_t length; /**< The length of the text in this piece */
        /** The buffer in memory for this piece if type == MEM,
         *  buf is NULL if type == FIL
         */
        char *buf;
        enum __buf_type type; /**< The type of this piece */
        /** A pointer to the piece containing the text after this piece */
        struct __piece *next;
        /** A pointer to the piece containing the text before this piece */
        struct __piece *prev;
        /** A pointer to the history_node for the insertion of this piece */
        history_node *insert_node;
};

/** Piece table data structure
 *  @see piecetab in piecetab.h
 */
struct __piecetab
{
        FILE *file_buffer; /**< The file buffer in the piece table */
        struct __piece *head; /**< The head of the list of pieces */
        size_t length; /**< The overall length of text in the piece table */
};

/** Initialises a struct __piece pointer
 *  @param node The struct __piece pointer to be initialised
 *  @param offset The value of the offset field in node
 *  @param length The value of the length field in node
 *  @param buf The value of the buf field in node
 *  @param type The value of the type field in node
 *  @param prev The value of the prev field in node
 *  @param next The value of the next field in node
 *  @param insert_node The value of the insert_node field in node
 *  @note The parameters passed to this function are inserted into node as is.
 *        Hence buf is only inserted into node as a pointer.
 */
static void
piece_init(node, offset, length, buf, type, prev, next, insert_node)
struct __piece *node;
size_t offset;
size_t length;
char *buf;
enum __buf_type type;
struct __piece *prev;
struct __piece *next;
history_node *insert_node;
{
        if (node != NULL)
        {
                node->offset = offset;
                node->length = length;
                node->buf = buf;
                node->type = type;
                node->prev = prev;
                node->next = next;
                node->insert_node = insert_node;
        }
}

/** Initialises a struct __piecetab
 *  @param file_ptr The file buffer for the new struct __piecetab
 *  @note file_ptr should refer to a valid open FILE *, or it should be NULL
 *  @return A pointer to the new struct __piecetab, or NULL upon error
 *  @see piecetab_init in piecetab.h
 */
struct __piecetab
*piecetab_init(file_ptr)
FILE *file_ptr;
{
        struct __piecetab *new_piecetab = malloc(sizeof(struct __piecetab));
        struct __piece *new_head;
        if (new_piecetab == NULL)
        {
                /* TODO report error */
                return NULL;
        }
        new_piecetab->file_buffer = file_ptr;
        if (file_ptr != NULL)
        {
                int status;
                status = fseek(file_ptr, 0, SEEK_END);
                if (status == -1)
                {
                        /* TODO report error */
                        free(new_piecetab);
                        return NULL;
                }
                new_piecetab->length = ftell(file_ptr);
                if (new_piecetab->length == -1)
                {
                        /* TODO report error */
                        free(new_piecetab);
                        return NULL;
                }
                new_head = malloc(sizeof(struct __piece));
                if (new_head == NULL)
                {
                        /* TODO report error */
                        free(new_piecetab);
                        return NULL;
                }
                piece_init(new_head,
                           0,
                           new_piecetab->length,
                           NULL,
                           FIL,
                           new_head,
                           new_head,
                           NULL);
        }
        else
        {
                new_head = NULL;
                new_piecetab->length = 0;
        }
        new_piecetab->head = new_head;
        return new_piecetab;
}

/** Frees the memory allocated to a struct __piecetab
 *  @param table A pointer to the struct __piecetab to free
 *  @note This function frees the pieces in table and their buffers
 *  @note Does not close table->file_buffer
 *  @note Runs free(table);
 *  @note Should be run when a struct __piecetab is no longer needed.
 *  @see piecetab_free in piecetab.h
 */
void
piecetab_free(table)
struct __piecetab *table;
{
        struct __piece *curr = table->head;
        if (curr != NULL)
        {
                do
                {
                        free(curr->buf);
                        curr = curr->next;
                }
                while (curr != table->head && curr != NULL);
        }
        free(table);
}

/** Initialises the left and right nodes formed when a FIL piece is split
 *  into two when text is inserted into it.
 *  @param lnode A pointer to the left node
 *  @param rnode A pointer to the right node
 *  @param index The index of the insertion
 *  @param pnode The node before the left node
 *  @param node The node between lnode and rnode
 *  @param nnode The next after the right node
 *  @return PIECETAB_ERR on error, PIECETAB_OK otherwise
 *  @note This function is just a helper for piece_insert_positive_lr
 */
static void
piece_insert_positive_fil(lnode, rnode, index, pnode, node, nnode)
struct __piece *lnode;
struct __piece *rnode;
size_t index;
struct __piece *pnode;
struct __piece *node;
struct __piece *nnode;
{
        if (node != NULL)
        {
                piece_init(lnode,
                           node->offset,
                           index,
                           NULL,
                           FIL,
                           pnode,
                           node,
                           NULL);
                piece_init(rnode,
                           index,
                           node->length - index,
                           NULL,
                           FIL,
                           node,
                           nnode,
                           NULL);
        }
}

/** Initialises the left and right nodes formed when a MEM piece is split
 *  into two when text is inserted into it.
 *  @param lnode A pointer to the left node
 *  @param rnode A pointer to the right node
 *  @param index The index of the insertion
 *  @param pnode The node before the left node
 *  @param node The node between lnode and rnode
 *  @param nnode The node after the right node
 *  @return PIECETAB_ERR on error, PIECETAB_OK otherwise
 *  @note This function is just a helper for piece_insert_positive_lr
 */
static int
piece_insert_positive_mem(lnode, rnode, index, pnode, node, nnode)
struct __piece *lnode;
struct __piece *rnode;
size_t index;
struct __piece *pnode;
struct __piece *node;
struct __piece *nnode;
{
        if (node != NULL)
        {
                piece_init(lnode,
                           0,
                           index,
                           malloc(lnode->length),
                           MEM,
                           pnode,
                           node,
                           NULL);
                if (lnode->buf == NULL)
                {
                        /* TODO cleanup and report error */
                        return PIECETAB_ERR;
                }
                
                piece_init(rnode,
                           0,
                           node->length - index,
                           malloc(rnode->length),
                           MEM,
                           node,
                           nnode,
                           NULL);
                if (rnode->buf == NULL)
                {
                        /* TODO cleanup and report error */
                        return PIECETAB_ERR;
                }

                memcpy(lnode->buf, node->buf, lnode->length);
                memcpy(rnode->buf, node->buf + index, rnode->length);
                free(node->buf);
                return PIECETAB_OK;
        }
        return PIECETAB_ERR;
}

/** Initialises the left and right nodes formed when a piece is split
 *  into two when an insertion is made in that piece.
 *  @param lnode A pointer to the left node
 *  @param rnode A pointer to the right node
 *  @param index The index of the insertion
 *  @param pnode The node before the left node
 *  @param node The node between lnode and rnode
 *  @param nnode The node after the right node
 *  @return PIECETAB_ERR on error, PIECETAB_OK otherwise
 *  @note This function is just a helper for piece_insert_positive
 */
static int
piece_insert_positive_lr(lnode, rnode, index, pnode, node, nnode)
struct __piece *lnode;
struct __piece *rnode;
size_t index;
struct __piece *pnode;
struct __piece *node;
struct __piece *nnode;
{
        if (node == NULL)
        {
                return PIECETAB_ERR;
        }
        switch (node->type)
        {
        case FIL:
                piece_insert_positive_fil(lnode,
                                          rnode,
                                          index,
                                          pnode,
                                          node,
                                          nnode);
                break;
        case MEM:
                int status = piece_insert_positive_mem(lnode,
                                                       rnode,
                                                       index,
                                                       pnode,
                                                       node,
                                                       nnode);
                if (status == PIECETAB_ERR)
                {
                        /* TODO cleanup and error reporting */
                        return PIECETAB_ERR;
                }
                break;
        default:
                /* TODO report error */
                return PIECETAB_ERR;
        }
        return PIECETAB_OK;
}

/** Inserts a string in the middle of a piece.
 *  @param node The node that the insertion is in the middle of
 *  @param index The index of the insertion
 *  @param str A buffer containing the string to be inserted
 *  @param length The length of the buffer
 *  @return A pointer to the piece containing str, or NULL on error
 *  @note The str pointer is inserted as is.
 */
static struct __piece
*piece_insert_positive(node, index, str, length)
struct __piece *node;
size_t index;
char *str;
size_t length;
{
        struct __piece *prev_node = node->prev;
        struct __piece *next_node = node->next;
        struct __piece *left_node;
        struct __piece *right_node;
        history_node *hist_node = node->insert_node;

        if (node == NULL ||
            str == NULL ||
            !length ||
            index >= node->length)
        {
                /* TODO report error */
                return NULL;
        }

        prev_node->next = malloc(sizeof(struct __piece));
        next_node->prev = malloc(sizeof(struct __piece));
        if (next_node->prev == NULL || prev_node->next == NULL)
        {
                /* TODO report error */
                free(prev_node->next);
                free(next_node->prev);
                prev_node->next = node;
                next_node->prev = node;
                return NULL;
        }

        left_node = prev_node->next;
        right_node = next_node->prev;
        status = piece_insert_positive_lr(left_node,
                                          right_node,
                                          index,
                                          prev_node,
                                          node,
                                          next_node);
        piece_init(node, 0, length, str, MEM, left_node, right_node, hist_node);

        if (hist_node == NULL)
        {
                int status = history_revise(hist_node, left_node, right_node);
                if (status == HISTORY_ERR)
                {
                        /* TODO report error */
                        /* TODO cleanup */
                        return NULL;
                }
        }
        return node;
}

/** Inserts a string after a piece.
 * @param node The node before insertion
 * @param str The buffer containing the string
 * @param length The length of the string
 * @return A pointer to the piece containing str, or NULL on error
 * @note The str pointer is inserted as is.
 */
static struct __piece
*piece_insert_end(node, str, length)
struct __piece *node;
char *str;
size_t length;
{
        struct __piece *new_node;
        if (node == NULL || str == NULL || !length)
        {
                /* TODO report error */
                return NULL;
        }

        new_node = malloc(sizeof(struct __piece));
        if (new_node == NULL)
        {
                /* TODO report error */
                return NULL;
        }
        
        piece_init(new_node, 0, length, str, MEM, node, node->next, NULL);
        node->next = new_node;
        return new_node;
}

/** Inserts a string before a piece.
 *  @param node The node after the insertion
 *  @param str The buffer containing the string
 *  @param length The length of the string
 *  @return A pointer to the piece containing str, or NULL on error
 *  @note The str pointer is inserted as is.
 */
static struct __piece
*piece_insert_zero(node, str, length)
struct __piece *node;
char *str;
size_t length;
{
        /* TODO make a generic error checking function for _end and _zero */
        struct __piece *new_node;
        if (node == NULL || str == NULL || !length)
        {
                /* TODO report error */
                return NULL;
        }

        new_node = malloc(sizeof(struct __piece));

        if (new_node == NULL)
        {
                /* TODO report error */
                return NULL;
        }
        
        piece_init(new_node, 0, length, str, MEM, node->prev, node, NULL);
        node->prev = new_node;
        return new_node;
}

/** Adjusts the index given to piece_insert to insert at the right place
 *  @param index The index to be adjusted
 *  @param node The node that the index is relative to
 *  @param result_ptr A pointer where the adjusted index will be stored
 *  @return Pointer to the node that *result_ptr is relative to, NULL on error
 */
static struct __piece
*adjust_index(index, node, result_ptr)
ssize_t index;
struct __piece *node;
size_t *result_ptr;
{
        if (result_ptr == NULL || node == NULL)
        {
                return NULL;
        }
        while (index < 0)
        {
                node = node->prev;
                index += node->length;
        }
        while (index > node->length)
        {
                index -= node->length;
                node = node->next;
        }
        *result_ptr = index;
        return node;
}

/** Inserts a string into a list of pieces
 *  @param node The node that index is relative to
 *  @param index The index of the insertion
 *  @param str The buffer containing the string to be inserted
 *  @param length The length of str
 *  @return The node that contains str, or NULL on error
 *  @note str is inserted as is as a pointer.
 */
static struct __piece
*piece_insert(node, index, str, length)
struct __piece *node;
ssize_t index;
char *str;
size_t length;
{
        struct __piece *result;
        size_t index_adj;
        struct __piece *temp_node;

        if (node == NULL || str == NULL)
        {
                /* TODO report error */
                return NULL;
        }

        temp_node = adjust_index(index, node, &index_adj);

        if (index_adj == temp_node->length)
        {
                result = piece_insert_end(temp_node, str, length);
        }
        else if (index_adj > 0)
        {
                result = piece_insert_positive(temp_node, index_adj, str, length);
        }
        else
        {
                result = piece_insert_zero(temp_node, str, length);
        }
        return result;
}

/** Inserts a string into a piecetab if the piecetab has no pieces in it
 *  @param table The struct __piecetab to be inserted into
 *  @param str The buffer containing the string to be inserted
 *  @param length The length of str
 *  @param hist The history struct that the insertion will be added to
 *  @return A pointer to the node containing str, or NULL on error
 */
static struct __piece
*piecetab_insert_empty(table, str, length, hist)
struct __piecetab *table;
char *str;
size_t length;
struct __piece *node;
history *hist;
{
        table->head = malloc(sizeof(struct __piece));
        if (table->head == NULL)
        {
                return NULL;
        }
        piece_init(table->head,
                   0,
                   length,
                   str,
                   MEM,
                   table->head,
                   table->head,
                   NULL);
        return table->head;
}

/** Inserts a string into a struct __piecetab
 *  @param table A pointer to the struct __piecetab to be inserted into
 *  @param str A buffer containing the string to be inserted
 *  @param index The index of the insertion relative to node
 *  @param length The length of str
 *  @param node A pointer to a struct __piece that index is relative to
 *  @param hist A pointer to a history struct to append this operation to
 *  @return A pointer to the node that contains str in table
 *  @note This function will store str (the pointer) as is.
 *  @note If node is not null, it will assume that it is the head,
 *        this is to allow for faster operations from a cursor position
 *  @see piecetab_insert in piecetab.h
 */
struct __piece
*piecetab_insert(table, str, index, length, node, hist)
struct __piecetab *table;
char *str;
ssize_t index;
size_t length;
struct __piece *node;
history *hist;
{
        struct __piece *curr;
        struct __piece *ref_point = node == NULL ? table->head : node;
        if (table == NULL || str == NULL || table->length + length < table_length)
        {
                /* TODO report error */
                return NULL;
        }
        if (length == 0)
        {
                return ref_point;
        }
        table->length += length;
        if (table->head != NULL)
        {
                curr = piece_insert(ref_point, index, str, length);
        }
        else
        {
                curr = piecetab_insert_empty(table, str, length, hist);
        }

        if (curr == NULL)
        {
                /* TODO report error and cleanup */
                return NULL;
        }
        if (hist != NULL)
        {
                history_node *hist_node = history_push(hist, INS, curr);
                if (hist_node == NULL)
                {
                        /* TODO report error and cleanup */
                        return NULL;
                }
                curr->insert_node = hist_node;
        }
        else
        {
                curr->insert_node = NULL;
        }
        return curr;
}

/* TODO retrieve and delete, all of history */
/* TODO document funcs and write tests */
