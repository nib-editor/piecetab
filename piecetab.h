#ifndef __NIB_PIECETAB_H

#define __NIB_PIECETAB_H

#include "history.h"

#define PIECETAB_OK 0

#define PIECETAB_ERR 1

typedef struct __piece piece;

typedef struct __piecetab piecetab;

piecetab *piecetab_init(FILE *);

void piecetab_free(piecetab *);

struct __piece *piecetab_insert(piecetab *,
                                char *,
                                long,
                                size_t,
                                piece *,
                                history *);

#endif
