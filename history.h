#ifndef __NIB_HISTORY_H

#define __NIB_HISTORY_H

#define HISTORY_OK 0

#define HISTORY_ERR 1

#include "piecetab.h"

typedef struct __history history;

typedef struct __history_node history_node;

history *history_init(void);

#endif
